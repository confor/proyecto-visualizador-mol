#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import listdir, path, system
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from parser import Parser
from shutil import which


class main:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('main.glade')

        def get(id):
            return self.builder.get_object(id)

        self.window = get('window')
        self.window.set_title('Visualizador de moléculas')
        self.window.set_default_size(800, 600)
        self.window.connect('destroy', Gtk.main_quit)
        self.window.show_all()

        self.choose_dir = get('choose_dir')
        self.choose_dir.connect('clicked', self.get_dir_selection)
        self.label_dir = get('current_dir')

        self.info_box = get('info')

        self.working_dir = '/'

        self.listmodel = Gtk.ListStore(str, str)
        self.table = self.builder.get_object('treeview')
        self.table.set_model(model=self.listmodel)

        cell = Gtk.CellRendererText()
        titles = ['Nombre', 'Formato']
        for i in range(len(titles)):
            col = Gtk.TreeViewColumn(titles[i], cell, text=i)
            self.table.append_column(col)

        self.update_tree()

        self.table.connect('row-activated', self.load_file)

        self.button_edit = get('modify')
        self.button_edit.connect('clicked', self.edit_file)

        self.image = get('image')
        self.can_show_images = False

        # check whether pymol is in $path
        if which('pymol') is not None:
            self.can_show_images = True

    def get_dir_selection(self, button=None):
        chooser = Gtk.FileChooserDialog(title='Seleccionar un directorio',
                                        action=Gtk.FileChooserAction.SELECT_FOLDER,
                                        buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))

        def handle_response(chooser, response):
            if response == Gtk.ResponseType.CANCEL:
                chooser.destroy()

            elif response == Gtk.ResponseType.OK:
                chosen = chooser.get_filename()
                if chosen is None:
                    return

                self.working_dir = chosen
                self.label_dir.set_text(' Seleccionado: ' + chosen)
                chooser.destroy()
                self.update_tree()

        chooser.connect('response', handle_response)
        chooser.run()

        return

    def update_tree(self):
        # clear table
        if len(self.listmodel) > 0:
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)

        # get every file from selected dir
        files = listdir(self.working_dir)

        if len(files) == 0:
            return

        filtered = []

        for file in files:
            # check if its a file
            if not path.isfile(path.join(self.working_dir, file)):
                continue

            # check if its .mol or .mol2
            if file.endswith(('.mol', '.mol2')):
                filtered.append(file)

        filtered.sort()

        # populate table
        for file in filtered:
            if file.endswith('.mol'):
                self.listmodel.append([file, 'MOL'])
            elif file.endswith('.mol2'):
                self.listmodel.append([file, 'MOL 2'])

    def get_table_selection(self):
        model, it = self.table.get_selection().get_selected()

        if model is None or it is None:
            return None

        name = model.get_value(it, 0)
        return name

    def load_file(self, treeview=None, treepath=None, column=None):
        name = self.get_table_selection()
        file = path.join(self.working_dir, name)
        format = 'Unknown'

        if name.endswith('.mol'):
            format = 'MOL'
        elif file.endswith('.mol2'):
            format = 'MOL2'

        if file is None:
            return

        parser = None

        try:
            handler = open(file, 'r')
            parser = Parser(handler)
            handler.close()

            text = 'Molécula: ' + name + '\n'

            if parser.version is not None and format != parser.version:
                text += 'Formato: ' + format + ' (' + parser.version.strip() + ')\n'
            else:
                text += 'Formato: ' + format + '\n'

            text += 'Átomos: ' + str(parser.atom_count) + '\n'
            text += 'Enlaces: ' + str(parser.bond_count) + '\n'

            if parser.atom_count > 0:
                text += '\nContenido:\n'

                atoms = {}
                total = 0

                for atom in parser.atoms:
                    total += 1

                    if 'atom_symbol' not in atom:
                        continue

                    if atom['atom_symbol'] not in atoms:
                        atoms[atom['atom_symbol']] = 1
                    else:
                        atoms[atom['atom_symbol']] += 1

                for key, value in atoms.items():
                    text += key + ': ' + str(int(value / total * 100)) + '%\n'

            self.info_box.set_text(text)

        except OSError as err:
            print('no se puede abrir la carpeta')
            print(err)
            return

        try:
            if self.can_show_images is False:
                print('No se puede renderizar la molécula: pymol no instalado')
                return

            # /tmp is guaranteed to exist in POSIX-compilant systems but not
            # all users might like using it, maybe because its ram-backed.
            # TODO use $TMPDIR or `mktmp`
            imagepath = '/tmp/pymol-render.png'
            returned = system('pymol -xcQ "' + file + '" -g "' + imagepath + '"')

            if returned == 0:
                self.image.set_from_file(imagepath)
            else:
                self.image.set_from_icon_name('gtk-missing-image')

        except TypeError:
            print('os.system explotó xd')
            return

    def edit_file(self, button=None):
        name = self.get_table_selection()

        if name is None:
            return

        file = path.join(self.working_dir, name)
        editor(name, file)


class editor:
    def __init__(self, name, file):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('editor.glade')

        def get(id):
            return self.builder.get_object(id)

        self.window = get('window')
        self.window.set_default_size(500, 600)
        # self.window.connect('destroy', self.cancel)
        self.window.show_all()

        self.info = get('info')
        self.content = get('content')
        self.button_cancel = get('cancel')
        self.button_save = get('save')

        self.name = name
        self.file = file
        self.load()

        self.info.set_text('Modificando ' + self.name)
        self.window.set_title('Editor: ' + self.name)

        self.button_cancel.connect('clicked', self.cancel)
        self.button_save.connect('clicked', self.save)

        return

    def cancel(self, button=None):
        self.window.close()
        self.window.destroy()

    def load(self):
        try:
            handler = open(self.file, 'r')
            data = handler.read()
            buffer = Gtk.TextBuffer()  # memory waste?
            buffer.set_text(data)
            self.content.set_buffer(buffer)
            handler.close()

        except OSError as err:
            self.content.set_text(':(')
            print('cant open file')
            print(err)

    def save(self, button=None):
        chooser = Gtk.FileChooserDialog(title='Guardar archivo...',
                                        action=Gtk.FileChooserAction.SAVE,
                                        buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))

        def handle_response(chooser, response):
            if response == Gtk.ResponseType.CANCEL:
                chooser.destroy()

            elif response == Gtk.ResponseType.OK:
                chosen = chooser.get_filename()
                if chosen is None:
                    return

                try:
                    buffer = self.content.get_buffer()
                    start = buffer.get_start_iter()
                    end = buffer.get_end_iter()
                    data = buffer.get_text(start, end, True)

                    handler = open(chosen, 'w+')
                    handler.write(data)
                    handler.close()
                    chooser.destroy()

                except OSError as err:
                    print('no se puede guardar el archivo :(')
                    print(chosen)
                    print(err)

        chooser.connect('response', handle_response)
        chooser.run()

        return
        return


if __name__ == '__main__':
    w = main()
    Gtk.main()
