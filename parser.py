from collections import OrderedDict


class Parser:
	# http://c4.cabrillo.edu/404/ctfile.pdf
	# http://chemyang.ccnu.edu.cn/ccb/server/AIMMS/mol2.pdf
	def __init__(self, handler):
		version = None
		lines = []
		counts = None

		line = handler.readline()
		content_line = 0

		while line:
			if ('$MDL' in line) or ('$$$$' in line) or ('$RXN' in line) or ('$RDFILE' in line):
				raise Exception('unsupported file format')

			if counts is None or version is None:
				content_line += 1

				if line.rstrip().endswith('V2000'):
					version = 'V2000'
					counts = line

				elif line.rstrip().endswith('V3000'):
					version = 'V3000'

				elif line.startswith(('@<TRIPOS>ATOM', '@<TRIPOS>MOLECULE', '@<TRIPOS>BOND')):
					version = 'MOL2'

				if version == 'V3000' and line.startswith('M  V30 COUNTS'):
					counts = line

			lines.append(line)
			line = handler.readline()

		# 3 * metadata + counts + atoms + bonds + properties + data + optional separator
		if len(lines) < 6:
			raise Exception('File is too small')

		self.name = None
		self.details = None
		self.comment = None

		self.atom_count = 0
		self.bond_count = 0
		self.atoms = []
		self.bonds = []
		self.version = version

		if version == 'V2000' or version == 'V3000':
			self.parse_mol(counts, lines, content_line)
			return

		elif version == 'MOL2':
			self.parse_mol2(lines)
			return

	def parse_mol(self, counts, lines, content_line):
		atoms = []
		bonds = []

		self.name = lines[0]
		self.details = lines[1]
		self.comment = lines[2]

		if self.version == 'V2000':
			self.atom_count = int(counts[3 * 0:3 * 1])
			self.bond_count = int(counts[3 * 1:3 * 2])
			# self.atom_list_count = counts[3 * 2:3 * 3]
			# self.fff = counts[3*3:3*4]
			# self.chiral = counts[3 * 4:3 * 5]
			# self.stext_entries = counts[3 * 5:3 * 6]
			# self.xxx = counts[3*6:3*7]
			# self.rrr = counts[3*7:3*8]
			# self.ppp = counts[3*8:3*9]
			# self.iii = counts[3*9:3*10]
			self.properties = counts[3 * 10:3 * 11]
			self.version = counts[3 * 11:3 * 13]

			for i in range(content_line, content_line + self.atom_count):
				# xxxxx.xxxxyyyyy.yyyyzzzzz.zzzz aaaddcccssshhhbbbvvvHHHrrriiimmmnnneee
				line = lines[i]
				atom = {}

				atom['raw'] = line
				# atom['x'] = float(line[0:10])
				# atom['y'] = float(line[10:20])
				# atom['z'] = float(line[20:30])

				atom['atom_symbol'] = str(line[31:34]).strip()
				# atom['mass_difference'] = line[34:36]
				# atom['charge'] = line[36:39]
				# atom['atom_stereo_parity'] = line[39:42]
				# atom['hydrogen_count'] = line[42:45]
				# atom['stereo_care_box'] = line[45:48]
				# atom['valence'] = line[48:51]
				# atom['h0_designator'] = line[51:54]
				# atom['rrr'] = line[54:57]
				# atom['iii'] = line[57:60]
				# atom['atom_atom_mapping_number'] = line[60:63]
				# atom['inversion_retention_flag'] = line[63:66]
				# atom['exact_change_flag'] = line[66:69]

				atoms.append(atom)

			for i in range(content_line + self.atom_count, content_line + self.atom_count + self.bond_count):
				bonds = lines[i]

			self.atoms = atoms
			self.bonds = bonds

			return

		elif self.version == 'V3000':
			cut = counts.split(' ')
			self.atom_count = int(cut[4])
			self.bond_count = int(cut[5])

			for i in range(content_line + 1, content_line + 1 + self.atom_count):
				line = lines[i].split(' ')
				atom = {}

				atom['raw'] = lines[i]
				atom['atom_symbol'] = line[4]

				atoms.append(atom)

			for i in range(content_line + 1 + self.atom_count + 1 + 1, content_line + 1 + self.atom_count + self.bond_count + 1 + 1):
				line = lines[i]
				bond = {}

				bond['raw'] = line
				# TODO get more info

				bonds.append(bond)

			self.atoms = atoms
			self.bonds = bonds

			return

	def parse_mol2(self, lines):
		sections = {}

		for line in lines:
			# section change
			if line.startswith('@<TRIPOS>') == True:
				section = line.strip().replace('@<TRIPOS>', '')
				continue

			# data store
			if section not in sections:
				sections[section] = []

			# remove newline
			if line.endswith('\r\n'):
				line = line[:-2]
			elif line.endswith('\n'):
				line = line[:-1]

			sections[section].append(line)

		if 'MOLECULE' in sections:
			section = sections['MOLECULE']
			self.name = section[0]

			counts = section[1].split()

			# wat
			self.atom_count = int(counts[0])

			if len(counts) >= 2:
				self.bond_count = int(counts[1])

			if len(counts) >= 3:
				self.structure_count = int(counts[2])

			if len(counts) >= 4:
				self.feature_count = int(counts[3])

			if len(counts) >= 5:
				self.set_count = int(counts[4])

			self.molecule_type = section[2]
			self.charge_type = section[3]

		if 'ATOM' in sections:
			atoms = []

			for line in sections['ATOM']:
				data = line.split()
				atom = {}

				atom['raw'] = line
				atom['id'] = data[0]
				atom['name'] = data[1]
				# atom['x'] = float(data[2].strip())
				# atom['y'] = float(data[3].strip())
				# atom['z'] = float(data[4].strip())
				atom['type'] = data[5]

				if '.' in atom['type']:
					atom['atom_symbol'] = atom['type'][:atom['type'].index('.')].strip()
				else:
					atom['atom_symbol'] = atom['type'].strip()
				# atom['substructure_id'] = data[6]
				# atom['substructure_name'] = data[7]
				# atom['charge'] = float(data[8])
				# atom['status'] = data[9]

				atoms.append(atom)

			self.atoms = atoms

		if 'BOND' in sections:
			bonds = []

			for line in sections['BOND']:
				bond = {}
				bond['raw'] = line
				bonds.append(bond)

			self.bonds = bonds

		return


# if __name__ == '__main__':
# 	# handler = open('ChEBI_16716.mol', 'r')
# 	# handler = open('388383.mol', 'r')
# 	handler = open('p3_2c.mol2', 'r')
# 	parser = Parser(handler)
# 	handler.close()

# 	trash = vars(parser)
# 	for key, value in trash.items():
# 		print(key, value, '\n\n')
